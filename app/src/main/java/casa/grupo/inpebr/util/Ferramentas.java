package casa.grupo.inpebr.util;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Process;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.IOException;
import java.io.InterruptedIOException;

import casa.grupo.inpebr.R;

/**
 * Created by Marcelo Martins on 02/08/2016.
 */
public class Ferramentas {
    MaterialDialog.Builder builder = null;
    MaterialDialog dialog = null;

    public void myTost(Context context, String mensagem) {
        Toast.makeText(context, mensagem, Toast.LENGTH_SHORT).show();
    }

    public void mySnake(View view, String mensagem){
       Snackbar.make(view, mensagem, Snackbar.LENGTH_LONG).show();
    }

    public void myDialog(Context contexto, String titulo, String mensagem) {
        builder = new MaterialDialog.Builder(contexto)
                .title(titulo)
                .content(mensagem);
        dialog = builder.build();
        openDialog();
    }

    public void myLoad(Context contexto, String titulo, String mensagem) {
        builder = new MaterialDialog.Builder(contexto)
                .title(titulo)
                .content(mensagem)
                .cancelable(false)
                .progress(true, 0);
        dialog = builder.build();
        openDialog();
    }

    private void openDialog(){
        if(dialog != null){
            dialog.show();
        }
    }

    public void closeDialog(){
        if(dialog != null){
            dialog.dismiss();
        }
    }

    public void closeKeybord(Activity activity) {
        try{
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isOnline(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }
}
