package casa.grupo.inpebr.util;

import android.os.AsyncTask;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import casa.grupo.inpebr.AsyncConectAPI;

/**
 * Created by Marcelo Martins on 01/08/2016.
 */
public class ConectAPI extends AsyncTask<HashMap, Void, JSONObject> {
    public AsyncConectAPI delegate = null;
    private final String BASE_API = "http://inpebr.com.br/API/";

    private JSONObject retorno = null;

    @Override
    protected JSONObject doInBackground(HashMap ... params) {
        try {
            Map<String, String> data = params[0];
            String controlador = BASE_API+data.get("controlador").toString();
            String json = HttpRequest.post(controlador).form(data).body();
            retorno = new JSONObject(json);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retorno;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        delegate.processFinish(jsonObject);
    }
}