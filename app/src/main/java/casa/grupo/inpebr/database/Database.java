package casa.grupo.inpebr.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import casa.grupo.inpebr.Bootstrap;

/**
 * Created by MarceloMartins on 8/5/16.
 */
public class Database {


    public static String ALTERNATIVA_RM() {
        String Retorno = "DROP TABLE IF EXISTS cad_alternativa";
        return Retorno.trim();
    }

    public static String ENTREVISTADO_RM() {
        String Retorno = "DROP TABLE IF EXISTS cad_entrevistado";
        return Retorno.trim();
    }

    public static String METRICA_RM() {
        String Retorno = "DROP TABLE IF EXISTS cad_metrica";
        return Retorno.trim();
    }

    public static String OPCOES_RM() {
        String Retorno = "DROP TABLE IF EXISTS cad_opcoes";
        return Retorno.trim();
    }

    public static String OPNIAO_RM() {
        String Retorno = "DROP TABLE IF EXISTS cad_opniao";
        return Retorno.trim();
    }

    public static String PERGUNTA_RM() {
        String Retorno = "DROP TABLE IF EXISTS cad_pergunta";
        return Retorno.trim();
    }

    public static String PESQUISA_RM() {
        String Retorno = "DROP TABLE IF EXISTS cad_pesquisa";
        return Retorno.trim();
    }

    public static String RESPOSTAS_RM() {
        String Retorno = "DROP TABLE IF EXISTS cad_respostas";
        return Retorno.trim();
    }

    public static String ALTERNATIVA_CR() {
        String Retorno = "";
        Retorno += "CREATE TABLE IF NOT EXISTS cad_alternativa (";
        Retorno += "   id INTEGER PRIMARY KEY,";
        Retorno += "   id_pergunta INTEGER,";
        Retorno += "   acao INTEGER,";
        Retorno += "   titulo VARCHAR";
        Retorno += ")";
        return Retorno.trim();
    }

    public static String ENTREVISTADO_CR() {
        String Retorno = "";
        Retorno += "CREATE TABLE IF NOT EXISTS cad_entrevistado (";
        Retorno += "    id INTEGER PRIMARY KEY AUTOINCREMENT,";
        Retorno += "    id_pesquisa INTEGER,";
        Retorno += "    id_pesquisador INTEGER,";
        Retorno += "    nome VARCHAR,";
        Retorno += "    cep VARCHAR,";
        Retorno += "    inicio datetime,";
        Retorno += "    fim datetime";
        Retorno += ")";
        return Retorno.trim();
    }

    public static String METRICA_CR() {
        String Retorno = "";
        Retorno += "CREATE TABLE IF NOT EXISTS cad_metrica (";
        Retorno += "    id_alternativa INTEGER PRIMARY KEY,";
        Retorno += "    quantidade INTEGER";
        Retorno += ")";
        return Retorno.trim();
    }

    public static String OPCOES_CR() {
        String Retorno = "";
        Retorno += "CREATE TABLE IF NOT EXISTS cad_opcoes(";
        Retorno += "    id INTEGER PRIMARY KEY,";
        Retorno += "    id_pergunta INTEGER,";
        Retorno += "    acao INTEGER,";
        Retorno += "    titulo VARCHAR";
        Retorno += ")";
        return Retorno.trim();
    }

    public static String OPNIAO_CR() {
        String Retorno = "";
        Retorno += "CREATE TABLE IF NOT EXISTS cad_opniao(";
        Retorno += "    id_entrevistado INTEGER PRIMARY KEY,";
        Retorno += "    id_pergunta INTEGER,";
        Retorno += "    resposta text";
        Retorno += ")";
        return Retorno.trim();
    }

    public static String PERGUNTA_CR() {
        String Retorno = "";
        Retorno += "CREATE TABLE IF NOT EXISTS cad_pergunta(";
        Retorno += "    id INTEGER PRIMARY KEY,";
        Retorno += "    id_pesquisa INTEGER,";
        Retorno += "    titulo VARCHAR,";
        Retorno += "    descricao text,";
        Retorno += "    alerta text,";
        Retorno += "    status VARCHAR,";
        Retorno += "    tipo INTEGER,";
        Retorno += "    obrigatoria INTEGER,";
        Retorno += "    metrica INTEGER";
        Retorno += ")";
        return Retorno.trim();
    }

    public static String PESQUISA_CR() {
        String Retorno = "";
        Retorno += "CREATE TABLE IF NOT EXISTS cad_pesquisa(";
        Retorno += "    id INTEGER PRIMARY KEY,";
        Retorno += "    titulo VARCHAR,";
        Retorno += "    descricao VARCHAR,";
        Retorno += "    numero VARCHAR,";
        Retorno += "    tipo INTEGER";
        Retorno += ")";
        return Retorno.trim();
    }

    public static String RESPOSTAS_CR() {
        String Retorno = "";
        Retorno += "CREATE TABLE IF NOT EXISTS cad_respostas(";
        Retorno += "    id INTEGER PRIMARY KEY,";
        Retorno += "    id_entrevistado INTEGER,";
        Retorno += "    id_alternativa INTEGER,";
        Retorno += "    id_opcao INTEGER";
        Retorno += ")";
        return Retorno.trim();
    }

    public void DatabaseInit(Context context) {
        try {

            SQLiteDatabase database = context.openOrCreateDatabase(Bootstrap.DATABASENAME, context.MODE_PRIVATE, null);

            database.execSQL(Database.RESPOSTAS_RM());
            database.execSQL(Database.OPNIAO_RM());
            database.execSQL(Database.ENTREVISTADO_RM());
            database.execSQL(Database.OPCOES_RM());
            database.execSQL(Database.ALTERNATIVA_RM());
            database.execSQL(Database.PERGUNTA_RM());
            database.execSQL(Database.METRICA_RM());
            database.execSQL(Database.PESQUISA_RM());

            database.execSQL(Database.PESQUISA_CR());
            database.execSQL(Database.METRICA_CR());
            database.execSQL(Database.PERGUNTA_CR());
            database.execSQL(Database.ALTERNATIVA_CR());
            database.execSQL(Database.OPCOES_CR());
            database.execSQL(Database.ENTREVISTADO_CR());
            database.execSQL(Database.OPNIAO_CR());
            database.execSQL(Database.RESPOSTAS_CR());

            System.out.println("CONSOLE - DATABASE CRIADO COM SUCESSO!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
