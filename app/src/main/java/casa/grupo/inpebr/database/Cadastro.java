package casa.grupo.inpebr.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONObject;

import casa.grupo.inpebr.Bootstrap;

/**
 * Created by MarceloMartins on 8/5/16.
 */
public class Cadastro {
    private String Tabela = null;
    private JSONObject Resultado = new JSONObject();
    private ContentValues Dados = new ContentValues();
    private static Context mContext = null;

    //Contrutor da classe
    public Cadastro(Context context) {
        this.mContext = context;
    }

    //Montando a query de cadastro
    public void ExeCadastro(String tabela, ContentValues dados) {
        this.Tabela = tabela;
        this.Dados = dados;
        this.Execute();
    }

    //Obtem a conexao a syntax e executa a query
    private void Execute() {
        SQLiteDatabase banco = mContext.openOrCreateDatabase(Bootstrap.DATABASENAME, mContext.MODE_PRIVATE, null);
        try {
            long Return = banco.insert(this.Tabela, null, this.Dados);
            Resultado.put("resultado", Return);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Retornando os resultados
    public JSONObject getResultado() {
        return Resultado;
    }
}
