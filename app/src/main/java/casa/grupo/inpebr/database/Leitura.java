package casa.grupo.inpebr.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONObject;

import casa.grupo.inpebr.Bootstrap;

/**
 * Created by MarceloMartins on 8/5/16.
 */
public class Leitura {
    private String Consulta;
    private JSONObject Resultado = new JSONObject();
    private static Context mContext = null;

    //Contrutor da classe
    public Leitura(Context context) {
        this.mContext = context;
    }

    //Montando a query de consulta
    public void ExeQuery(String query) {
        this.Consulta = query;
        this.Execute();
    }

    //Montando a query de consulta
    public void ExeLeitura(String tabela) {
        this.Consulta = "SELECT * FROM " + tabela;
        this.Execute();
    }

    //Montando a query de consulta com termos
    public void ExeLeitura(String tabela, String termos) {
        this.Consulta = "SELECT * FROM " + tabela + " WHERE " + termos;
        this.Execute();
    }

    //Obtem a conexao a syntax e executa a query
    private void Execute() {
        SQLiteDatabase banco = mContext.openOrCreateDatabase(Bootstrap.DATABASENAME, mContext.MODE_PRIVATE, null);
        try {
            Cursor cursor = banco.rawQuery(this.Consulta, null);
            Result(cursor);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Montando JSONObject de retorno
    public void Result(Cursor cursor) {
        try {
            JSONArray resultSet = new JSONArray();
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                int totalColumn = cursor.getColumnCount();
                JSONObject rowObject = new JSONObject();
                for (int i = 0; i < totalColumn; i++) {
                    if (cursor.getColumnName(i) != null) {
                        try {
                            rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                resultSet.put(rowObject);
                cursor.moveToNext();
            }
            Resultado.put("resultado", resultSet);
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Retornando os resultados
    public JSONObject getResultado() {
        return Resultado;
    }
}
