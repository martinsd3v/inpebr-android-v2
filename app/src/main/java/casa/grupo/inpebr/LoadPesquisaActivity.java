package casa.grupo.inpebr;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.logging.Handler;

import casa.grupo.inpebr.database.Cadastro;
import casa.grupo.inpebr.database.Deleta;
import casa.grupo.inpebr.database.Leitura;
import casa.grupo.inpebr.util.ConectAPI;
import casa.grupo.inpebr.util.Ferramentas;

public class LoadPesquisaActivity extends AppCompatActivity implements AsyncConectAPI {

    private LoadPesquisaActivity mContext = LoadPesquisaActivity.this;
    private View mView;

    Ferramentas ferramentas = new Ferramentas();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_pesquisa);

        mView = findViewById(android.R.id.content);

        Toolbar mToobar = (Toolbar) findViewById(R.id.my_toolbar);
        mToobar.setTitle(getString(R.string.title_load_pesquisa));
        setSupportActionBar(mToobar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button mButton = (Button) findViewById(R.id.buttonId);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText pesuisa = (EditText) findViewById(R.id.identificadorId);
                String Pesquisa = pesuisa.getText().toString();

                HashMap data = new HashMap();
                data.put("controlador", "Android.php");
                data.put("acao", "Leitura");
                data.put("id", Pesquisa);

                //Efetuando conexão com o servidor
                if (ferramentas.isOnline(mContext)) {
                    //Iniciando dialog de carregamento
                    ferramentas.myLoad(mContext, "Carregando informações", "Por favor aguarde...");

                    ConectAPI conexao = new ConectAPI();
                    conexao.delegate = mContext;
                    conexao.execute(data);
                } else {
                    ferramentas.mySnake(mView, mContext.getString(R.string.msg_erro_conexao));
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    @Override
    public void processFinish(JSONObject resposta) {
        try {
            if (resposta != null) {
                if (resposta.getInt("status") == 2) {
                    if (!resposta.isNull("pesquisa")) {//Verificando se retornou alguma pesquisa
                        JSONObject pesquisaAPI = (JSONObject) resposta.get("pesquisa");//Pesquisa retornada pela API
                        new cadastraPesquisas().execute(pesquisaAPI);
                    } else {//Pesquisa não encontrada
                        ferramentas.mySnake(mView, "Pesquisa não encontrada");
                    }
                } else if (resposta.getInt("status") == 1) {//Erro retornado pela API
                    ferramentas.closeDialog();
                    ferramentas.mySnake(mView, resposta.get("mensagem").toString());
                }
            } else {//Deu Erro na api
                ferramentas.mySnake(mView, mContext.getString(R.string.msg_erro_server));
            }
        } catch (Exception e) {
            e.printStackTrace();
            ferramentas.closeDialog();
            ferramentas.mySnake(mView, mContext.getString(R.string.msg_erro_json));
        }
    }

    private class cadastraPesquisas extends AsyncTask<JSONObject, Void, Boolean> {
        protected Boolean doInBackground(JSONObject... objects) {
            JSONObject pesquisaAPI = (JSONObject) objects[0];

            try {
                Leitura leitura = new Leitura(mContext);
                leitura.ExeLeitura("cad_pesquisa", " id = " + pesquisaAPI.getString("id"));

                JSONArray pesquisaCadastrada = (JSONArray) leitura.getResultado().get("resultado");

                if (pesquisaCadastrada.isNull(0)) {//Pesquisa ainda não cadastrada

                    ContentValues Pesquisa = new ContentValues();
                    Pesquisa.put("id", pesquisaAPI.getString("id"));
                    Pesquisa.put("titulo", pesquisaAPI.getString("titulo"));
                    Pesquisa.put("descricao", pesquisaAPI.getString("descricao"));
                    Pesquisa.put("numero", pesquisaAPI.getString("numero"));
                    Pesquisa.put("tipo", pesquisaAPI.getString("tipo"));

                    Cadastro cad_pesquisa = new Cadastro(mContext);
                    cad_pesquisa.ExeCadastro("cad_pesquisa", Pesquisa);

                    if (!pesquisaAPI.isNull("perguntas")) {//Verificando se retornou alguma pergunta referente a pesquisa
                        JSONArray perguntasAPI = pesquisaAPI.getJSONArray("perguntas"); //Array com todas as perguntas referente a pesquisa

                        for (int i = 0; i < perguntasAPI.length(); i++) {//efetuando looping nas perguntas da pesquisa atual
                            JSONObject perguntaAPI = (JSONObject) perguntasAPI.get(i);//Pergunta dentro do looping

                            if(perguntaAPI.getString("status").equals("1")) {
                                ContentValues Pergunta = new ContentValues();
                                Pergunta.put("id", perguntaAPI.getString("id"));
                                Pergunta.put("id_pesquisa", perguntaAPI.getString("id_pesquisa"));
                                Pergunta.put("titulo", perguntaAPI.getString("titulo"));
                                Pergunta.put("descricao", perguntaAPI.getString("descricao"));
                                Pergunta.put("alerta", perguntaAPI.getString("alerta"));
                                Pergunta.put("status", perguntaAPI.getString("status"));
                                Pergunta.put("tipo", perguntaAPI.getString("tipo"));
                                Pergunta.put("obrigatoria", perguntaAPI.getString("obrigatoria"));
                                Pergunta.put("metrica", perguntaAPI.getString("metrica"));

                                Cadastro cad_pergunta = new Cadastro(mContext);
                                cad_pergunta.ExeCadastro("cad_pergunta", Pergunta);

                                if (!perguntaAPI.getString("tipo").equals("5")) {//Perguntas com altrenativas
                                    if (!perguntaAPI.isNull("alternativas")) {//Verificando se retornou alguma pesquisa
                                        JSONArray alternativasAPI = perguntaAPI.getJSONArray("alternativas"); //Array com todas as perguntas referente a pesquisa

                                        for (int a = 0; a < alternativasAPI.length(); a++) {//efetuando looping nas alternativas da pergunta atual
                                            JSONObject alternativaAPI = (JSONObject) alternativasAPI.get(a);//Alternativa dentro do looping

                                            ContentValues Alternativa = new ContentValues();
                                            Alternativa.put("id", alternativaAPI.getString("id"));
                                            Alternativa.put("id_pergunta", alternativaAPI.getString("id_pergunta"));
                                            Alternativa.put("acao", alternativaAPI.getString("acao"));
                                            Alternativa.put("titulo", alternativaAPI.getString("titulo"));

                                            Cadastro cad_alternativa = new Cadastro(mContext);
                                            cad_alternativa.ExeCadastro("cad_alternativa", Alternativa);
                                        }

                                        if (perguntaAPI.getString("tipo").equals("3") || perguntaAPI.getString("tipo").equals("4")) {//Perguntas com opcoes
                                            if (!perguntaAPI.isNull("opcoes")) {//Verificando se retornou alguma pesquisa
                                                JSONArray opcoesAPI = perguntaAPI.getJSONArray("opcoes"); //Array com todas as perguntas referente a pesquisa
                                                //AQUI CADASTRA AS ALTERNATIVAS REFERENTE A PERGUNTA

                                                //efetuando looping nas alternativas da pergunta atual
                                                for (int l = 0; l < opcoesAPI.length(); l++) {
                                                    JSONObject opcaoAPI = (JSONObject) opcoesAPI.get(l);//Alternativa dentro do looping

                                                    ContentValues Opcao = new ContentValues();
                                                    Opcao.put("id", opcaoAPI.getString("id"));
                                                    Opcao.put("id_pergunta", opcaoAPI.getString("id_pergunta"));
                                                    Opcao.put("acao", opcaoAPI.getString("acao"));
                                                    Opcao.put("titulo", opcaoAPI.getString("titulo"));

                                                    Cadastro cad_opcoes = new Cadastro(mContext);
                                                    cad_opcoes.ExeCadastro("cad_opcoes", Opcao);
                                                }
                                            }
                                        }

                                    }
                                }
                            }

                        }

                    }

                    //ferramentas.myTost(mContext, "Pesquisa carregada com sucesso");
                } else {//Pesquisa já existente no aplicativo
                    //ferramentas.myTost(mContext, "Está pesquisa já adicionada anteriormente");
                }

                Deleta deleta = new Deleta(mContext);
                deleta.ExeDeleta("cad_metrica", "id_alternativa != '' ");
                if (!pesquisaAPI.isNull("metricas")) {//Verificando se retornou alguma metrica
                    JSONArray metricasAPI = pesquisaAPI.getJSONArray("metricas"); //Array com todas as metricas

                    for (int m = 0; m < metricasAPI.length(); m++) {//efetuando looping nas metrocas
                        JSONObject metricaAPI = (JSONObject) metricasAPI.get(m);//Metrica dentro do looping

                        ContentValues Metrica = new ContentValues();
                        Metrica.put("id_alternativa", metricaAPI.getString("id_alternativa"));
                        Metrica.put("quantidade", metricaAPI.getString("quantidade"));

                        Cadastro cad_metrica = new Cadastro(mContext);
                        cad_metrica.ExeCadastro("cad_metrica", Metrica);
                    }
                }

                SharedPreferences appPreferences = mContext.getSharedPreferences(Bootstrap.APPPREFERENCE, 0);
                SharedPreferences.Editor editor = appPreferences.edit();
                editor.putString("pesquisa_id", pesquisaAPI.getString("id"));
                editor.commit();

                Intent intent = new Intent(mContext, MainActivity.class);
                startActivity(intent);
                finish();

            } catch (Exception e) {
                e.printStackTrace();
                ferramentas.mySnake(mView, mContext.getString(R.string.msg_erro_json));
            }
            return true;
        }

        protected void onPostExecute(Boolean result) {
            ferramentas.closeDialog();
        }
    }
}
