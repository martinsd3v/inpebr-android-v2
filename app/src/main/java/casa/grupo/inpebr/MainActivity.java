package casa.grupo.inpebr;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import casa.grupo.inpebr.database.Database;
import casa.grupo.inpebr.fragments.InicaPesquisaFragment;
import casa.grupo.inpebr.fragments.PadraoFragment;
import casa.grupo.inpebr.fragments.QuestionarioFragment;

public class MainActivity extends AppCompatActivity {

    //iniciando atributos
    private MainActivity mContext = MainActivity.this;
    private View mView;
    private Toolbar mToobar;

    public static String usuario_id = "";
    public static String pesquisa_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mView = findViewById(android.R.id.content);
        mToobar = (Toolbar) findViewById(R.id.my_toolbar);
        mToobar.setTitle(R.string.app_name);
        setSupportActionBar(mToobar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Verifica se existe banco de dados local se não existir criar o mesmo
        SharedPreferences databasePreferences = getSharedPreferences(Bootstrap.APPPREFERENCE, 0);
        if (!databasePreferences.contains("database")) {
            new Database().DatabaseInit(mContext);

            //BANCO DE DADOS CRIADO SALVAR ENTÃO
            SharedPreferences.Editor editor = databasePreferences.edit();
            editor.putString("database", Bootstrap.DATABASENAME);
            editor.commit();
        }

        //Verificando se existe ou não pesquisador logado no aplicativo
        SharedPreferences appPreferences = getSharedPreferences(Bootstrap.APPPREFERENCE, 0);
        if (appPreferences.contains("pesquisa_id") && appPreferences.contains("usuario_id")) {
            this.fragmentPesquisa();

            pesquisa_id = appPreferences.getString("pesquisa_id", "");
            usuario_id = appPreferences.getString("usuario_id", "");

        } else {
            this.fragmentPadrao();
            if(appPreferences.contains("usuario_id")) usuario_id = appPreferences.getString("usuario_id", null);
        }
    }

    public void fragmentPesquisa(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        InicaPesquisaFragment inicaPesquisaFragment = new InicaPesquisaFragment();
        fragmentTransaction.replace(R.id.recebeFragment, inicaPesquisaFragment);
        fragmentTransaction.commit();
    }

    public void fragmentPadrao(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        PadraoFragment padraoFragment = new PadraoFragment();
        fragmentTransaction.replace(R.id.recebeFragment, padraoFragment);
        fragmentTransaction.commit();
    }

    public void fragmentQuestionario(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        QuestionarioFragment questionarioFragment = new QuestionarioFragment();
        fragmentTransaction.replace(R.id.recebeFragment, questionarioFragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_send_pesquisa:
                SincronizaRespostas sincronizaRespostas = new SincronizaRespostas(mContext, mView);
                sincronizaRespostas.sincronizar();
                break;
            case R.id.action_login_activity:
                Intent loginIntent = new Intent(mContext, LoginActivity.class);
                startActivity(loginIntent);
                break;
            case R.id.action_load_pesquisa:
                if (!MainActivity.usuario_id.equals("")) {
                    Intent LoadIntent = new Intent(mContext, LoadPesquisaActivity.class);
                    startActivity(LoadIntent);
                } else {
                    Intent LoginIntent = new Intent(mContext, LoginActivity.class);
                    LoginIntent.putExtra("loadPesquisa", true);
                    startActivity(LoginIntent);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
    }
}
