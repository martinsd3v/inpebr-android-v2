package casa.grupo.inpebr.fragments;


import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import casa.grupo.inpebr.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PadraoFragment extends Fragment {
    private TextView appVersion;

    public PadraoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_padrao, container, false);

        PackageInfo packageInfo = null;

        try{
            packageInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
        }catch (PackageManager.NameNotFoundException e){
            e.printStackTrace();
        }

        appVersion = (TextView) mView.findViewById(R.id.aap_version);
        appVersion.setText(getString(R.string.app_version, packageInfo.versionName));

        return mView;
    }

}
