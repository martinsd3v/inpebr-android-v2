package casa.grupo.inpebr.fragments;


import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import casa.grupo.inpebr.Bootstrap;
import casa.grupo.inpebr.MainActivity;
import casa.grupo.inpebr.R;
import casa.grupo.inpebr.database.Cadastro;
import casa.grupo.inpebr.database.Leitura;
import casa.grupo.inpebr.util.Ferramentas;

/**
 * A simple {@link Fragment} subclass.
 */
public class QuestionarioFragment extends Fragment {

    private LinearLayout mLayoutAlternativas;
    private Button mRegistrar;
    private TextView mPassos;
    private TextView mTipo;
    private TextView mPergunta;
    private View mView;
    private Toolbar mToobar;

    private int indicePergunta = 0;
    private JSONArray perguntaArray = null;

    private boolean metrica = false;
    private boolean perguntaFinal = false;
    private boolean obrigatoria = false;
    private int perguntaTipo = 0;
    private Integer perguntaId = 0;

    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private Date dataInicial = new Date();

    //Criando arrayList para armazenar
    private ArrayList<Integer> alternativasPerguntaOpcoes = new ArrayList<Integer>();

    JSONObject perguntasAlternativasResposta = new JSONObject();

    public QuestionarioFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_questionario, container, false);

        //btn Avançar
        mRegistrar = (Button) mView.findViewById(R.id.registrar);
        mRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    boolean valido = false;
                    if (obrigatoria) {
                        JSONObject perguntaAtual = perguntasAlternativasResposta.getJSONObject(perguntaId.toString());
                        if (perguntaTipo == 1) {//Alternativas Única Escolha
                            JSONObject respostaAtual = perguntaAtual.getJSONObject("resposta");
                            if (!respostaAtual.get("alternativa").equals("")) valido = true;
                        } else if (perguntaTipo == 2) {//Alternativas Multipla Escolha
                        } else if (perguntaTipo == 3) {//Alternativas + Opções Única Escolha
                            JSONArray respostaOpcoes = perguntaAtual.getJSONArray("resposta");
                            for (int ro = 0; ro < respostaOpcoes.length(); ro++) {
                                JSONObject alternativas = respostaOpcoes.getJSONObject(ro);
                                if (alternativas.getString("opcao").equals("")) {
                                    valido = false;
                                    break;
                                } else {
                                    valido = true;
                                }
                            }
                        } else if (perguntaTipo == 4) {//Alternativas + Opções Multipla Escolha
                        } else if (perguntaTipo == 5) {//Está é uma pergunta Dicertativa
                            JSONObject respostaOpniao = perguntaAtual.getJSONObject("resposta");
                            String opniao = respostaOpniao.getString("opniao");
                            if (opniao.length() > 1) valido = true;
                        }
                    } else {
                        valido = true;
                    }

                    if (valido) {
                        if (perguntaFinal) {
                            salvarEntrevista();
                        } else {
                            carregaPergunta();
                        }
                    } else {
                        new Ferramentas().myDialog(getContext(), "Pergunta obrigatoria", "Não é possivel pular está pergunta");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        //Verifica se existe preferencias de usuário e de pesquisa
        if (!MainActivity.usuario_id.equals("") && !MainActivity.pesquisa_id.equals("")) {
            try {
                Leitura leitura = new Leitura(getContext());
                leitura.ExeLeitura("cad_pesquisa", "id = " + MainActivity.pesquisa_id);
                JSONArray pesquisatArray = leitura.getResultado().getJSONArray("resultado");

                if (pesquisatArray.length() > 0) {
                    JSONObject pesquisaObject = pesquisatArray.getJSONObject(0);
                    leitura.ExeLeitura("cad_pergunta", "  id_pesquisa = " + pesquisaObject.get("id"));
                    perguntaArray = leitura.getResultado().getJSONArray("resultado");

                    //Montando objeto com todas as perguntas
                    for (int p = 0; p < perguntaArray.length(); p++) {
                        JSONObject perguntaObject = perguntaArray.getJSONObject(p);
                        if (Integer.parseInt(perguntaObject.getString("tipo")) == 1) {
                            JSONObject opcaoEscolhida = new JSONObject();
                            opcaoEscolhida.put("alternativa", "");

                            JSONObject resposta = new JSONObject();
                            resposta.put("tipo", perguntaObject.getString("tipo"));
                            resposta.put("resposta", opcaoEscolhida);
                            perguntasAlternativasResposta.put(perguntaObject.getString("id"), resposta);
                        } else if (Integer.parseInt(perguntaObject.getString("tipo")) == 3) {
                            leitura.ExeLeitura("cad_alternativa", "id_pergunta = " + perguntaObject.getString("id"));
                            JSONArray alternativaArray = leitura.getResultado().getJSONArray("resultado");

                            if (alternativaArray.length() > 0) {
                                JSONArray alternativas = new JSONArray();
                                for (int a = 0; a < alternativaArray.length(); a++) {
                                    JSONObject alternativaObject = alternativaArray.getJSONObject(a);

                                    JSONObject opcaoEscolhida = new JSONObject();
                                    opcaoEscolhida.put("alternativa", alternativaObject.getString("id"));
                                    opcaoEscolhida.put("opcao", "");
                                    alternativas.put(opcaoEscolhida);
                                }
                                JSONObject resposta = new JSONObject();
                                resposta.put("tipo", perguntaObject.getString("tipo"));
                                resposta.put("resposta", alternativas);
                                perguntasAlternativasResposta.put(perguntaObject.getString("id"), resposta);
                            }
                        } else if (Integer.parseInt(perguntaObject.getString("tipo")) == 5) {
                            JSONObject opniao = new JSONObject();
                            opniao.put("opniao", "");
                            JSONObject resposta = new JSONObject();
                            resposta.put("tipo", perguntaObject.getString("tipo"));
                            resposta.put("resposta", opniao);
                            perguntasAlternativasResposta.put(perguntaObject.getString("id"), resposta);
                        }
                    }

                    if (perguntaArray.length() > 0) carregaPergunta();
                } else {
                    limpaPreferencias();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            //Se não existir as preferencias então resetar as preferencias
            limpaPreferencias();
        }
        return mView;
    }

    private void limpaPreferencias() {
        SharedPreferences appPreferences = getActivity().getSharedPreferences(Bootstrap.APPPREFERENCE, 0);
        SharedPreferences.Editor editor = appPreferences.edit();
        editor.putString("usuario_nome", "");
        editor.putString("usuario_id", "");
        editor.commit();

        Intent intent = new Intent(getContext(), MainActivity.class);
        startActivity(intent);
    }

    private void salvarEntrevista() {
        try {
            Date dataFinal = new Date();

            //Cadastrando um entrevistado
            ContentValues entrevistado = new ContentValues();
            entrevistado.put("id_pesquisa", MainActivity.pesquisa_id);
            entrevistado.put("id_pesquisador", MainActivity.usuario_id);
            entrevistado.put("inicio", dateFormat.format(dataInicial));
            entrevistado.put("fim", dateFormat.format(dataFinal));
            Cadastro cadastro = new Cadastro(getContext());
            cadastro.ExeCadastro("cad_entrevistado", entrevistado);
            if (cadastro.getResultado().getInt("resultado") > 0) {
                Integer entrevistadoId = cadastro.getResultado().getInt("resultado");

                System.out.println("Entrevistado " + cadastro.getResultado());
                Iterator<?> keys = perguntasAlternativasResposta.keys();
                while (keys.hasNext()) {
                    String key = (String) keys.next();
                    JSONObject pergunta = perguntasAlternativasResposta.getJSONObject(key);
                    System.out.println("Pergunta: " +key +" Resposta: " +pergunta);

                    if (pergunta.getString("tipo").equals("1")) {//Pergunta de unica escolha
                        JSONObject resposta = pergunta.getJSONObject("resposta");
                        if(!resposta.getString("alternativa").equals("")) {
                            ContentValues respostaPergunta = new ContentValues();
                            respostaPergunta.put("id_entrevistado", entrevistadoId);
                            respostaPergunta.put("id_alternativa", resposta.getString("alternativa"));
                            cadastro.ExeCadastro("cad_respostas", respostaPergunta);
                        }
                    } else if (pergunta.getString("tipo").equals("3")) {//Pergunta de unica escolha
                        JSONArray resposta = pergunta.getJSONArray("resposta");
                        for (int r = 0; r < resposta.length(); r++){
                            JSONObject respostaAlternativas = resposta.getJSONObject(r);
                            if(!respostaAlternativas.getString("opcao").equals("")) {
                                ContentValues respostaPergunta = new ContentValues();
                                respostaPergunta.put("id_entrevistado", entrevistadoId);
                                respostaPergunta.put("id_alternativa", respostaAlternativas.getString("alternativa"));
                                respostaPergunta.put("id_opcao", respostaAlternativas.getString("opcao"));
                                cadastro.ExeCadastro("cad_respostas", respostaPergunta);
                            }
                        }
                    } else if (pergunta.getString("tipo").equals("5")) {//Pergunta de unica escolha
                        JSONObject resposta = pergunta.getJSONObject("resposta");
                        if(!resposta.getString("opniao").equals("")) {
                            ContentValues respostaPergunta = new ContentValues();
                            respostaPergunta.put("id_entrevistado", entrevistadoId);
                            respostaPergunta.put("id_pergunta", key);
                            respostaPergunta.put("resposta", resposta.getString("opniao"));
                            cadastro.ExeCadastro("cad_opniao", respostaPergunta);
                        }
                    }
                }

                new Ferramentas().myTost(getContext(), "Pesquisa registrada");
                Intent intent = new Intent(getContext(), MainActivity.class);
                startActivity(intent);
            }else{
                //Erro ao cadastrar entrevistador
                new Ferramentas().myDialog(getContext(), "Ocorrel um erro", "Não foi possivel cadastrar entrevista.");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void carregaPergunta() {
        if (indicePergunta + 1 == perguntaArray.length()) {
            perguntaFinal = true;
            mRegistrar.setText("Finalizar pesquisa");
        }

        //Verifica se o indice é maior doque a quantidade de perguntas

        mLayoutAlternativas = (LinearLayout) mView.findViewById(R.id.layout_alternativas);
        mPassos = (TextView) mView.findViewById(R.id.passos);
        mTipo = (TextView) mView.findViewById(R.id.tipo);
        mPergunta = (TextView) mView.findViewById(R.id.pergunta);

        //Reset preferencias
        metrica = false;
        obrigatoria = false;

        //Instanciando elementos
        TextView textView;
        final EditText editText;
        RadioButton radioButton;
        RadioGroup radioGroup;
        View view;
        Leitura leitura = new Leitura(getContext());

        mLayoutAlternativas.removeAllViews();//Limpando layout

        try {
            JSONObject perguntaObject = perguntaArray.getJSONObject(indicePergunta);//Recuperando informaçoes do banco de dados

            perguntaTipo = Integer.parseInt(perguntaObject.getString("tipo"));
            perguntaId = Integer.parseInt(perguntaObject.getString("id"));
            if (perguntaObject.getString("metrica").equals("2")) metrica = true;
            if (perguntaObject.getString("obrigatoria").equals("1")) obrigatoria = true;

            String[] tipo = new String[10];
            tipo[1] = "Alternativas Única Escolha";
            tipo[2] = "Alternativas Multipla Escolha";
            tipo[3] = "Alternativas + Opções Única Escolha";
            tipo[4] = "Alternativas + Opções Multipla Escolha";
            tipo[5] = "Está é uma pergunta Dicertativa";

            mPassos.setText("Pergunta  " + (indicePergunta + 1) + " de " + (perguntaArray.length()));
            mPergunta.setText(perguntaObject.getString("titulo"));
            mTipo.setText("(" + tipo[perguntaTipo] + ")");

            leitura.ExeLeitura("cad_alternativa", "id_pergunta = " + perguntaObject.getString("id"));
            JSONArray alternativaArray = leitura.getResultado().getJSONArray("resultado");

            if (perguntaTipo == 1) {//Alternativas Única Escolha
                if (alternativaArray.length() > 0) {
                    //Criando elemento para armazenar todos os radiosButtons
                    radioGroup = new RadioGroup(getContext());
                    radioGroup.clearCheck();
                    radioGroup.setOrientation(RadioGroup.VERTICAL);

                    //Cliando radioButtons dinamicamente
                    for (int a = 0; a < alternativaArray.length(); a++) {
                        JSONObject alternativaObject = alternativaArray.getJSONObject(a);
                        radioButton = new RadioButton(getContext());
                        radioButton.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        radioButton.setText(alternativaObject.getString("titulo"));
                        radioButton.setId(Integer.parseInt(alternativaObject.getString("id")));
                        radioGroup.addView(radioButton);
                    }

                    //Adicionando os radiosbuttons ao layout
                    mLayoutAlternativas.addView(radioGroup);

                    //Setantando evento para recuperar alternativa selecionada
                    radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup radioGroup, int i) {
                            try {
                                JSONObject perguntaAtual = perguntasAlternativasResposta.getJSONObject(perguntaId.toString());
                                JSONObject respostaAtual = perguntaAtual.getJSONObject("resposta");
                                respostaAtual.put("alternativa", radioGroup.getCheckedRadioButtonId());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            } else if (perguntaTipo == 3) {//Alternativas + Opções Única Escolha
                alternativasPerguntaOpcoes.clear();//Limpando arrayList responsavel por armazenar alternativas da pergunta

                if (alternativaArray.length() > 0) {
                    leitura.ExeLeitura("cad_opcoes", "id_pergunta = " + perguntaObject.getString("id"));
                    JSONArray opcoesArray = leitura.getResultado().getJSONArray("resultado");

                    for (int a = 0; a < alternativaArray.length(); a++) {
                        JSONObject alternativaObject = alternativaArray.getJSONObject(a);
                        alternativasPerguntaOpcoes.add(alternativaObject.getInt("id"));

                        LinearLayout.LayoutParams layoutParans = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 1);
                        layoutParans.setMargins(0, 10, 0, 20);

                        view = new View(getContext());
                        view.setLayoutParams(layoutParans);
                        view.setBackgroundColor(Color.parseColor("#636161"));
                        mLayoutAlternativas.addView(view);

                        textView = new TextView(getContext());
                        textView.setText(alternativaObject.getString("titulo"));
                        mLayoutAlternativas.addView(textView);

                        radioGroup = new RadioGroup(getContext());
                        radioGroup.setTag(Integer.parseInt(alternativaObject.getString("id")));
                        radioGroup.setOrientation(RadioGroup.VERTICAL);

                        for (int o = 0; o < opcoesArray.length(); o++) {
                            JSONObject opcoesObject = opcoesArray.getJSONObject(o);
                            radioButton = new RadioButton(getContext());
                            radioButton.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            radioButton.setText(opcoesObject.getString("titulo"));
                            radioButton.setId(Integer.parseInt(opcoesObject.getString("id")));
                            radioGroup.addView(radioButton);
                        }
                        mLayoutAlternativas.addView(radioGroup);
                        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                                try {
                                    JSONObject perguntaLoop = perguntasAlternativasResposta.getJSONObject(perguntaId.toString());
                                    JSONArray perguntaAlternativas = perguntaLoop.getJSONArray("resposta");
                                    for (int pa = 0; pa < perguntaAlternativas.length(); pa++) {
                                        JSONObject respostaAtual = perguntaAlternativas.getJSONObject(pa);
                                        if (respostaAtual.getString("alternativa").equals(radioGroup.getTag().toString())) {
                                            respostaAtual.put("opcao", radioGroup.getCheckedRadioButtonId());
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            } else if (perguntaTipo == 5) {//Está é uma pergunta Dicertativa
                LinearLayout.LayoutParams layoutParans = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParans.setMargins(0, 5, 0, 0);

                editText = new EditText(getContext());
                editText.setLayoutParams(layoutParans);
                mLayoutAlternativas.addView(editText);

                editText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        JSONObject perguntaAtual = null;
                        try {
                            perguntaAtual = perguntasAlternativasResposta.getJSONObject(perguntaId.toString());
                            JSONObject respostaAtual = perguntaAtual.getJSONObject("resposta");
                            respostaAtual.put("opniao", editText.getText());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }

            indicePergunta++;

            ScrollView mScrollView;
            mScrollView = (ScrollView) mView.findViewById(R.id.myScrol);
            mScrollView.fullScroll(ScrollView.FOCUS_UP);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}