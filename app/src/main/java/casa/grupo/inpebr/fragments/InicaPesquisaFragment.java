package casa.grupo.inpebr.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import casa.grupo.inpebr.Bootstrap;
import casa.grupo.inpebr.MainActivity;
import casa.grupo.inpebr.R;
import casa.grupo.inpebr.database.Leitura;


/**
 * A simple {@link Fragment} subclass.
 */
public class InicaPesquisaFragment extends Fragment {

    private TextView mTitulo;
    private Button buttonIniciaId;

    public InicaPesquisaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_inica_pesquisa, container, false);

        buttonIniciaId = (Button) mView.findViewById(R.id.buttonIniciaId);
        buttonIniciaId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).fragmentQuestionario();
            }
        });

        if (!MainActivity.usuario_id.equals("") && !MainActivity.pesquisa_id.equals("")) {
            try {
                Leitura leitura = new Leitura(getContext());
                leitura.ExeLeitura("cad_pesquisa", "id = " + MainActivity.pesquisa_id);
                JSONArray resultArray = (JSONArray) leitura.getResultado().get("resultado");
                if (resultArray.length() > 0) {
                    JSONObject resultObject = (JSONObject) resultArray.get(0);

                    mTitulo = (TextView) mView.findViewById(R.id.pesquisa_titulo);
                    mTitulo.setText("Nº "+resultObject.getString("numero"));

                } else {
                    limpaPreferencias();                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            limpaPreferencias();
        }
        return mView;
    }

    private void limpaPreferencias(){
        SharedPreferences appPreferences = getActivity().getSharedPreferences(Bootstrap.APPPREFERENCE, 0);
        SharedPreferences.Editor editor = appPreferences.edit();
        editor.putString("usuario_nome", "");
        editor.putString("usuario_id", "");
        editor.commit();

        Intent intent = new Intent(getContext(), MainActivity.class);
        startActivity(intent);
    }
}
