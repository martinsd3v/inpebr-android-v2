package casa.grupo.inpebr;

import android.content.Context;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import casa.grupo.inpebr.database.Leitura;
import casa.grupo.inpebr.util.ConectAPI;
import casa.grupo.inpebr.util.Ferramentas;

/**
 * Created by MarceloMartins on 8/16/16.
 */
public class SincronizaRespostas implements AsyncConectAPI {
    Context mContext;
    View mView;

    Ferramentas ferramentas = new Ferramentas();

    public SincronizaRespostas(Context context, View view) {
        mContext = context;
        mView = view;
    }

    public void sincronizar() {
        try {
            Leitura leitura = new Leitura(mContext);

            leitura.ExeLeitura("cad_entrevistado", " id_pesquisa = " + MainActivity.pesquisa_id);
            JSONArray entrevistadosArray = leitura.getResultado().getJSONArray("resultado");
            if (entrevistadosArray.length() > 0) {
                JSONArray entrevistadosSend = new JSONArray();
                for (int e = 0; e < entrevistadosArray.length(); e++) {
                    JSONObject entrevistadoObject = entrevistadosArray.getJSONObject(e);
                    JSONObject entrevistadoSend = new JSONObject();
                    System.out.println("Entrevistado " + entrevistadoObject);
                    entrevistadoSend.put("id_app", entrevistadoObject.getString("id"));
                    entrevistadoSend.put("id_pesquisador", entrevistadoObject.getString("id_pesquisador"));
                    entrevistadoSend.put("id_pesquisa", entrevistadoObject.getString("id_pesquisa"));
                    entrevistadoSend.put("inicio", entrevistadoObject.getString("inicio"));
                    entrevistadoSend.put("fim", entrevistadoObject.getString("fim"));

                    //Lendo respostas deste entrevistado
                    leitura.ExeLeitura("cad_respostas", "id_entrevistado = " + entrevistadoObject.getString("id"));
                    JSONArray respostasArray = leitura.getResultado().getJSONArray("resultado");
                    if (respostasArray.length() > 0) {
                        JSONArray respostasSend = new JSONArray();
                        for (int r = 0; r < respostasArray.length(); r++) {
                            JSONObject respostaObject = respostasArray.getJSONObject(r);
                            JSONObject respostaSend = new JSONObject();
                            System.out.println("cad resposta " + respostaObject);
                            respostaSend.put("id_alternativa", respostaObject.getString("id_alternativa"));
                            if (!respostaObject.isNull("id_opcao")) {
                                respostaSend.put("id_opcao", respostaObject.getString("id_opcao"));
                            }
                            respostasSend.put(r, respostaSend);
                        }
                        entrevistadoSend.put("respostas", respostasSend);
                    }

                    //Lendo opnioes deste entrevistado
                    leitura.ExeLeitura("cad_opniao", "id_entrevistado = " + entrevistadoObject.getString("id"));
                    JSONArray opnioesArray = leitura.getResultado().getJSONArray("resultado");
                    if (opnioesArray.length() > 0) {
                        JSONArray opnioesSend = new JSONArray();
                        for (int r = 0; r < opnioesArray.length(); r++) {
                            JSONObject opniaoObject = opnioesArray.getJSONObject(r);
                            JSONObject opniaoSend = new JSONObject();
                            opniaoSend.put("id_pergunta", opniaoObject.getString("id_pergunta"));
                            opniaoSend.put("resposta", opniaoObject.getString("resposta"));
                            opnioesSend.put(r, opniaoSend);
                        }
                        entrevistadoSend.put("opnioes", opnioesSend);
                    }
                    entrevistadosSend.put(e, entrevistadoSend);
                }

                HashMap data = new HashMap();
                data.put("controlador", "Android.php");
                data.put("acao", "Sincronismo");
                data.put("respostas", entrevistadosSend);

                //Efetuando conexão com o servidor
                if (ferramentas.isOnline(mContext)) {
                    //Iniciando dialog de carregamento
                    ferramentas.myLoad(mContext, "Enviando informações", "Por favor aguarde...");
                    ConectAPI conexao = new ConectAPI();
                    conexao.delegate = SincronizaRespostas.this;
                    conexao.execute(data);
                } else {
                    ferramentas.mySnake(mView, mContext.getString(R.string.msg_erro_conexao));
                }
            } else {
                ferramentas.mySnake(mView, "Nenhum registro encontrado");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void processFinish(JSONObject resposta) {
        ferramentas.myTost(mContext, "Registros enviados com sucesso");
        ferramentas.closeDialog();
    }
}
