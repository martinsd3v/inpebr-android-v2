package casa.grupo.inpebr;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import casa.grupo.inpebr.util.ConectAPI;
import casa.grupo.inpebr.util.Ferramentas;

public class LoginActivity extends AppCompatActivity implements AsyncConectAPI {

    //Intanciando classes que serão utilizadas
    Ferramentas ferramentas = new Ferramentas();

    //iniciando atributos
    private LoginActivity mContext = LoginActivity.this;
    private View mView;
    private Toolbar mToobar;
    private EditText login;
    private EditText senha;
    private Button acessar;
    private Boolean LoadPesquisa = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            LoadPesquisa = true;
        }

        mView = findViewById(android.R.id.content);

        mToobar = (Toolbar) findViewById(R.id.my_toolbar);
        mToobar.setTitle(getString(R.string.title_login_pesquisa));
        setSupportActionBar(mToobar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Recuperando informações
        login = (EditText) findViewById(R.id.loginId);
        senha = (EditText) findViewById(R.id.senhaId);
        acessar = (Button) findViewById(R.id.buttonId);

        acessar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Recuperando textos digitados
                String Login = login.getText().toString();
                String Senha = senha.getText().toString();

                //Montando HashMap para enviar os paramentros corretamente
                HashMap data = new HashMap();
                data.put("controlador", "Pesquisadores.php");
                data.put("acao", "Login");
                data.put("login", Login);
                data.put("senha", Senha);

                //Efetuando conexão com o servidor
                if (ferramentas.isOnline(mContext)) {
                    //Iniciando dialog de carregamento
                    ferramentas.myLoad(mContext, "Carregando informações", "Por favor aguarde...");

                    ConectAPI conexao = new ConectAPI();
                    conexao.delegate = mContext;
                    conexao.execute(data);
                } else {
                    ferramentas.mySnake(mView, getString(R.string.msg_erro_conexao));
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        ferramentas.mySnake(mView, getString(R.string.msg_restrito));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        System.out.println("ENTROU NO BACK");
        onBackPressed();
        return true;
    }

    @Override
    public void processFinish(JSONObject resposta) {
        try {
            if (resposta != null) {
                if (resposta.getInt("status") == 2) {
                    //Deu certo
                    ferramentas.closeDialog();

                    JSONObject respostaAPI = resposta.getJSONObject("resposta");

                    SharedPreferences appPreferences = getSharedPreferences(Bootstrap.APPPREFERENCE, 0);
                    SharedPreferences.Editor editor = appPreferences.edit();
                    editor.putString("usuario_nome", respostaAPI.getString("nome"));
                    editor.putString("usuario_id", respostaAPI.getString("id"));
                    editor.commit();

                    //ferramentas.myTost(mContext, "Bem vindo "+respostaAPI.getString("nome"));
                    if (LoadPesquisa) {
                        Intent intent = new Intent(mContext, LoadPesquisaActivity.class);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(mContext, MainActivity.class);
                        startActivity(intent);
                    }
                } else if (resposta.getInt("status") == 1) {
                    //Deu erro
                    ferramentas.closeDialog();
                    ferramentas.mySnake(mView, resposta.get("mensagem").toString());
                }
            } else {
                //Deu Erro na api
                ferramentas.closeDialog();
                ferramentas.mySnake(mView, getString(R.string.msg_erro_server));
            }
        } catch (JSONException e) {
            ferramentas.closeDialog();
            ferramentas.mySnake(mView, getString(R.string.msg_erro_json));
            e.printStackTrace();
        }
    }
}