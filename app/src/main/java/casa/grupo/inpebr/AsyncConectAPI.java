package casa.grupo.inpebr;

import org.json.JSONObject;

/**
 * Created by Marcelo Martins on 02/08/2016.
 */

public interface AsyncConectAPI {
    void processFinish(JSONObject resposta);
}
